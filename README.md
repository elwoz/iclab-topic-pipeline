# ICLab Topic Pipeline

This repository holds all of the software behind
https://iclab-tagteam.cs.umass.edu/ , except for TagTeam itself (for
which see https://github.com/berkmancenter/tagteam ).

## Roadmap

The top-level directories are as follows:

 * [`input-feeds`](input-feeds): Programs which
   periodically retrieve reports of censorship, or potential
   censorship, and format them as RSS feeds for consumption by
   TagTeam.

 * ... more coming soon ...

## Contributing

We welcome contributions from anyone, anywhere.  Contributions that
would be especially useful at this time include:

 * Additional input-feed programs
 * ... more coming soon ...

Please make contributions as pull requests to this repository.

Please note that, by making a contribution, you are agreeing for it to
be redistributed to the public under the license terms below.

## Licensing

All of the files in this repository, whether or not they bear an
explicit copyright notice and license grant, are copyright
2018--present all of the contributors to this Git repository, plus
others as noted in specific files, and are licensed to the public
under the GNU General Public License, version 3 (or, at your option,
any later version).  See the file [`COPYING3`](COPYING3)
in this repository for the exact terms of the GPL.
