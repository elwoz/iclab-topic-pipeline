"""
Utility routines having to do with URLs.
"""

import re
from urllib.parse import urlsplit, SplitResult
from socket import AF_INET, AF_INET6, inet_aton, inet_pton, inet_ntop

_enap_re = re.compile(br'[\x00-\x20\x7F-\xFF]|'
                      br'%(?!(?:[0-9A-Fa-f]{2}|u[0-9A-Fa-f]{4}))')
def _encode_nonascii_and_percents(segment):
    """Percent-encode all characters in SEGMENT that are not already ASCII,
       and all percent signs that are not already part of a correct
       percent-encoding sequence (%xx or %uxxxx)."""
    segment = segment.encode("utf-8", "surrogateescape")
    return _enap_re.sub(
        lambda m: "%{:02X}".format(ord(m.group(0))).encode("ascii"),
        segment).decode("ascii")

_fix_prefix_re = re.compile(r'^(https?\b)?[:/]*', re.IGNORECASE|re.ASCII)
def _fix_prefix(url):
    """URL has something wrong with its prefix, such that urlsplit produced
       a SplitResult with an empty netloc.  Heuristically fix this up:

          foo.bar        http://foo.bar
          foo.bar/baz    http://foo.bar/baz
          http/foo.bar   http://foo.bar

        Several other variations on the theme of incorrect punctuation
        after a leading 'http' or 'https' are also accepted.
    """
    m = _fix_prefix_re.match(url)
    if m:
        return (m.group(1) or 'http') + '://' + url[m.end():]
    return url

def _canon_hostname(name):
    """Canonicalize a hostname or IP address literal as follows:

       - If NAME's first character is '[' and its last character is ']',
         then it must be an IPv6 address literal; if it is acceptable
         to inet_pton(AF_INET6), then it is canonicalized (by inet_ntop)
         and returned, otherwise it is invalid.

       - If NAME is any string acceptable to the legacy inet_aton()
         parser, then it is taken as an IPv4 address literal,
         canonicalized (by inet_ntop) and returned.

       - Otherwise, NAME is assumed to be a domain name and is
         IDNA-encoded, which has the side-effect of rejecting invalid
         domain names.
    """
    if not name:
        raise ValueError('empty hostname')
    if name[0] == '[' and name[-1] == ']':
        return '[' + inet_ntop(AF_INET6, inet_pton(AF_INET6, name[1:-1])) + ']'
    try:
        return inet_ntop(AF_INET, inet_aton(name))
    except OSError:
        pass
    return name.encode("idna").decode("ascii")

_iplit_re = re.compile(
    r'^(?:\[[0-9a-fA-F:.]+\]|[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})$')
def _is_domain_name(name):
    """True if NAME appears to be a domain name rather than an IP address
       literal.  Only precise for names that have already been
       processed by _canon_hostname.
    """
    return not _iplit_re.match(name)

def canon_url_syntax(url, *, want_splitresult=None):
    """Syntactically canonicalize a URL.  This makes the following
       transformations:
         - if the scheme is missing (foo.bar.com or //foo.bar.com),
           it is set to 'http'
         - scheme and hostname are lowercased
         - hostname is canonicalized
         - user and password fields are stripped
         - vacuous port fields are stripped
         - ports redundant to the scheme are also stripped
         - path becomes '/' if empty
         - characters outside the printable ASCII range in path,
           query, fragment, user, and password are %-encoded, as are
           improperly used % signs

       You can provide either a string or a SplitResult, and you get
       back what you put in.  You can set the optional argument
       want_splitresult to True or False to force a particular
       type of output.
    """

    try:
        if isinstance(url, SplitResult):
            if want_splitresult is None: want_splitresult = True
            exploded = url

        else:
            if want_splitresult is None: want_splitresult = False

            if not isinstance(url, str):
                url = url.decode('utf-8', 'surrogateescape')

            exploded = urlsplit(url)
            if not exploded.netloc:
                exploded = urlsplit(_fix_prefix(url))

        if not exploded.hostname:
            raise ValueError("url with no host")

        scheme = exploded.scheme
        if scheme == '':
            scheme = 'http'
        if scheme != "http" and scheme != "https":
            raise ValueError("url with non-http(s) scheme")

        host   = _canon_hostname(exploded.hostname)
        port   = exploded.port
        path   = _encode_nonascii_and_percents(exploded.path)
        query  = _encode_nonascii_and_percents(exploded.query)
        frag   = _encode_nonascii_and_percents(exploded.fragment)

        if path == "":
            path = "/"

        if port is None:
            port = ""
        elif ((port == 80  and scheme == "http") or
              (port == 443 and scheme == "https")):
            port = ""
        else:
            port = ":{}".format(port)

        netloc = host + port

        result = SplitResult(scheme, netloc, path, query, frag)
        if want_splitresult:
            return result
        else:
            return result.geturl()

    except Exception as e:
        raise ValueError("syntax error in url {!r}".format(url)) from e

def url_variants(url, *,
                 vary_scheme=True, vary_www=True, vary_siteroot=True,
                 want_splitresult=None):
    """Expand a URL into up to eight possible variant URLs:

         http://       host (/path)
         https://      host (/path)
         http://  www. host (/path)
         https:// www. host (/path)

       If the path component is already just '/', then of course it
       doesn't change.  If 'www.' is already present in the hostname,
       then we try removing it; if it isn't there, we try adding it.
       If the hostname is actually an IP address, then we don't vary
       it.  Each of the three alternations can be disabled by passing
       False for vary_scheme, vary_www, or vary_siteroot.

       As with canon_url_syntax, you can provide either a string or a
       SplitResult, and you get back what you put in.  You can set the
       optional argument want_splitresult to True or False to force a
       particular type of output.
    """
    if want_splitresult is None:
        want_splitresult = isinstance(url, SplitResult)

    su = canon_url_syntax(url, want_splitresult=True)

    schemes = [su.scheme]
    if vary_scheme:
        schemes.append("http" if su.scheme == "https" else "https")

    netlocs = [su.netloc]
    if vary_www and _is_domain_name(su.hostname):
        if su.port:
            port = ':{}'.format(su.port)
        else:
            port = ''
        if su.hostname.startswith("www."):
            netlocs.append(su.hostname[4:] + port)
        else:
            netlocs.append("www." + su.hostname + port)

    pqfs = [ (su.path, su.query, su.fragment) ]
    if vary_siteroot:
        if su.path != '/' or su.query != '' or su.fragment != '':
            pqfs.append(('/', '', ''))

    for s in schemes:
        for n in netlocs:
            for p, q, f in pqfs:
                su = SplitResult(s, n, p, q, f)
                if want_splitresult:
                    yield su
                else:
                    yield su.geturl()
