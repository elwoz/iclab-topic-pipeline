# -*- coding: utf-8 -*-
from lxml import html
import requests
import urllib2
from bs4 import BeautifulSoup
import re
from pymongo import MongoClient
mongoclient = MongoClient("localhost:27018")
db = mongoclient.churn


def tag_generator(iclab_entry):
    iclab_tags = ''
    print iclab_entry
    country = iclab_entry['country']
    blockpage = iclab_entry['block']
    dns = iclab_entry['dns']
    rst = iclab_entry['rst']
    seq = iclab_entry['seq']
    ttl = iclab_entry['ttl']
    if country is not None:
    if blockpage is True:
        iclab_tags = iclab_tags + 'iclab-blockpage-'+country + ','
    if dns is True:
        iclab_tags = iclab_tags + 'iclab-dnstamper-'+country + ','
    if rst is True:
        iclab_tags = iclab_tags + 'iclab-rst-'+country + ','
    if seq is True:
        iclab_tags = iclab_tags + 'iclab-seq-'+country + ','
    if ttl is not None:
        iclab_tags = iclab_tags + 'iclab-ttl-'+country + ','
    else:
	iclab_tags = iclab_tags + 'iclab-nodata' + ','

    return iclab_tags[:-1]
s = requests.session()

# get auth token
url = 'https://iclab-tagteam.cs.umass.edu/accounts/sign_in'
page = s.get(url)

parsed_html = BeautifulSoup(page.text,'html.parser')

authenticity_token = parsed_html.body.find('input', attrs={'name':'authenticity_token'})['value']


# login to get session cookie
username = 'arian'
password = '123456m.'
data={'authenticity_token': authenticity_token, 'commit': 'Log+in', 'user[login]':username, 'user[password]':password, 'user[remember_me]':'0', 'utf8':'✓'}


r = s.post("https://iclab-tagteam.cs.umass.edu/accounts/sign_in",data=data)
items_url = 'https://iclab-tagteam.cs.umass.edu/hubs/test/items'
items_page = s.get(items_url)
parsed_csrf = BeautifulSoup(items_page.text,'html.parser')


csrf_token = re.findall(r'.*name="csrf-token"', str(parsed_csrf.head))


csrf_token = csrf_token[0].replace('" name="csrf-token"','').replace('<meta content="','')

print 'received csrf token ', csrf_token


# fetch the ids from the json

page = 1
while True:
    json_url = 'https://iclab-tagteam.cs.umass.edu/hubs/test/items.json?page='+str(page)
    page += 1
    json_file = s.get(json_url)
    json_file_json = json_file.json()
    try:
	for it in json_file_json['feed_items']:
    	    print it['id'] , it['url']
    	    iclab_res = db.anon.find({'url':it['url']})
    	    iclab_tags = ''
	    if (iclab_res.count()) > 0:
		iclab_tags = tag_generator(iclab_res[0])
    	    else:
	        iclab_tags = 'iclab-nodata'
    	    data_tag = {'filter_type':'AddTagFilter', 'new_tag': ''+str(iclab_tags),'csrfmiddlewaretoken':csrf_token}
    	    s.cookies['bookmarklet_hub_choice'] = '2'
    	    tag_add = s.post('https://iclab-tagteam.cs.umass.edu/hubs/test/feed_items/'+str(it['id'])+'/tag_filters', data=data_tag, headers = {'Referer':items_url, 'X-CSRF-Token':csrf_token})

    except Exception as exp:
	print str(exp)
	break