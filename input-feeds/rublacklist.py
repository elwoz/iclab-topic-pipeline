#! /usr/bin/python3
#
# Copyright 2018 Zachary Weinberg.
# This program is free software; you may copy, modify, and distribute
# it under the terms of the GNU General Public License, version 3 (or,
# at your option, any later version).  See the file COPYING3 at the
# root of this source repository for exact license terms, or consult
# https://www.gnu.org/licenses/gpl.html

"""Scrape and filter the РосКомСвобода "registry of prohibited sites"
censored in Russia, and generate an RSS feed suitable for import to
TagTeam.
"""

import configparser
import datetime
import html
import os
import sqlite3
import sys
from   urllib.parse import urljoin, urlsplit

from   bs4 import BeautifulSoup
import requests
import feedgenerator

EPOCH = datetime.datetime.utcfromtimestamp(0)

class Config:
    def __init__(self, extra_cfgs):
        parser = configparser.ConfigParser(
            strict=True,
            interpolation=None,
            default_section=None) # this only sorta disables DEFAULT
                                  # but it's the best we can do
        with open("input-feeds.cfg") as f:
            parser.read_file(f)
        parser.read(extra_cfgs)

        common = parser["common"]
        rubl   = parser["rublacklist"]

        self.source      = rubl["source"]
        self.max_entries = int(rubl["max_entries"])
        self.dest_f      = os.path.join(common["output_dir"], rubl["dest"])
        self.cache_f     = os.path.join(common["cache_dir"],  rubl["cache"])

class Cache:
    """The persistent cache of reports.  Wraps a SQLite database.
       No database queries should happen outside this class.
    """
    def __init__(self, cfg):
        self._conn = None
        self._cur  = None
        self._max_entries = cfg.max_entries
        self._cache_f = cfg.cache_f

    def __enter__(self):
        self._conn = sqlite3.connect(self._cache_f,
                                    detect_types=sqlite3.PARSE_DECLTYPES,
                                    uri=False)
        self._conn.row_factory = sqlite3.Row
        self._cur = self._conn.cursor()

        # The cache database may or may not have just been created;
        # initialize it if necessary.
        # Unfortunately there's no "create table or, if it exists,
        # throw an error if it doesn't match this schema" option.
        with self._conn:
            cur = self._cur
            cur.executescript("""
                CREATE TABLE IF NOT EXISTS misc (
                    key   TEXT NOT NULL PRIMARY KEY,
                    value TEXT NOT NULL
                ) WITHOUT ROWID;

                CREATE TABLE IF NOT EXISTS reports (
                    host     TEXT NOT NULL PRIMARY KEY,
                    date     TIMESTAMP NOT NULL,
                    agencies TEXT NOT NULL,
                    regid    INTEGER
                ) WITHOUT ROWID;

                -- This index is used by get_most_recent_reports.
                CREATE INDEX IF NOT EXISTS reports_idx_cc_date ON reports
                  (date DESC);
            """)

        return self

    def __exit__(self, *dontcare):
        self._cur.close()
        self._conn.close()
        return False

    def front_last_modified(self):
        with self._conn:
            cur = self._cur
            cur.execute("SELECT value FROM misc WHERE key = 'Last-Modified';")
            rows = cur.fetchall()
            if not rows:
                return None
            assert len(rows) == 1 and len(rows[0]) == 1
            return rows[0][0]

    def set_front_last_modified(self, last_modified):
        with self._conn:
            cur = self._cur
            cur.execute("""
                INSERT OR REPLACE INTO misc VALUES('Last-Modified', ?);
            """, (last_modified,))

    def most_recent_report_id(self):
        with self._conn:
            cur = self._cur
            cur.execute("""
               SELECT regid FROM reports ORDER BY date DESC LIMIT 1
            """)
            rows = cur.fetchall()
            if rows:
                assert len(rows) == 1 and len(rows[0]) == 1
                return str(rows[0][0])
            else:
                return None

    def record_new_reports(self, reports):
        cur = self._cur
        with self._conn:
            for report in reports:
                host   = report["host"]
                date   = report["date"]
                agency = html.escape(report["agency"]).replace(",", "&#44;")
                regid  = int(report["regid"])

                cur.execute("""
                   SELECT date, agencies, regid FROM reports WHERE host = ?
                """, (host,))
                rows = cur.fetchall()
                if not rows:
                    cur.execute("""
                        INSERT INTO reports VALUES (?, ?, ?, ?);
                    """, (host, date, agency, regid))

                else:
                    assert len(rows) == 1 and len(rows[0]) == 3
                    row = rows[0]

                    if agency:
                        agencies = set(row["agencies"].split(","))
                        agencies.add(agency)
                        agencies = ",".join(sorted(agencies))
                    if row["date"] > date:
                        regid = row["regid"]
                        date  = row["date"]

                    cur.execute("""
                        UPDATE reports
                           SET date = ?, regid = ?, agencies = ?
                         WHERE host = ?;
                    """, (date, regid, agencies, host))

        # Possibly run ANALYZE here, rather than before closing
        # the connection (as the SQLite manual recommends), because
        # no further writes to the database will happen in this run,
        # and a complicated read query is about to happen.
        cur.execute("PRAGMA optimize;")

    def get_most_recent_reports(self):
        with self._conn:
            cur = self._cur
            cur.execute("""
               SELECT host, date, agencies, regid
                 FROM reports
             ORDER BY date DESC
                LIMIT ?;
            """, (self._max_entries,))

            yield from cur

#
# Parsing
#
def get_next_page(soup):
    # "Вперед" = "Forward"
    # bs4 wants unquoted > to look for them in text nodes
    forward = soup.find(string="Вперед >>")
    if forward:
        return forward.parent["href"]
    return None

def parse_reports(soup):
    for tr in soup.find_all("tr"):
        cells = tr.find_all("td")
        if len(cells) < 6:
            continue

        # The first cell should contain an image.  We expect this
        # image's URL to be either /static/image/o.png, which is a
        # closed lock, or /static/image/x.png, which is an open lock.
        # Drop rows with an open lock.
        if cells[0].img['src'] != '/static/image/o.png':
            continue

        # The second cell holds the date.
        date = datetime.datetime.strptime("".join(cells[1].stripped_strings),
                                          "%d.%m.%Y")

        # The third cell holds the hostname of interest, and a link to a
        # details page.
        link = cells[2].a
        regid = link["href"].split("/")[2]
        host = "".join(link.stripped_strings)
        if not host:
            continue

        # The "hostname" might actually be a full URL, or it might be
        # a wildcard hostname.
        if host.startswith("*."):
            host = host[2:]
        elif host.startswith("http://") or host.startswith("https://"):
            host = urlsplit(host).hostname
        elif '/' in host:
            host = host.split('/')[0]

        # In all cases, forcibly convert to IDNA representation.
        host = host.encode('idna').decode('ascii')

        # The fourth cell is a list of IP addresses, which we don't
        # care about.

        # The fifth cell contains the name of the agency that decreed
        # that this site should be blocked.
        agency = "".join(cells[4].stripped_strings)

        # The sixth cell is the number of "Доменов, заблокированных за
        # компанию" which I think means "domains blocked in company"
        # (i.e. number of domains blocked at the same time as this one);
        # we don't need this information.

        yield {
            "host": host,
            "regid": regid,
            "agency": agency,
            "date": date
        }

def retrieve_new_reports(cfg, cache):
    req_headers = {
        "User-Agent": "iclab-inputs/1.0 (zackw at cmu dot edu)"
    }
    lastmod = cache.front_last_modified()
    if lastmod is not None:
        req_headers["If-Modified-Since"] = lastmod

    most_recent_id = cache.most_recent_report_id()
    new_reports = []
    first = True
    stop = False
    url = cfg.source
    while len(new_reports) < cfg.max_entries:
        resp = requests.get(url, headers=req_headers)
        resp.raise_for_status()
        if resp.status_code == 304:
            break
        if first:
            if "Last-Modified" in resp.headers:
                cache.set_front_last_modified(resp.headers["Last-Modified"])
            if lastmod is not None:
                del req_headers["If-Modified-Since"]
            first = False

        soup = BeautifulSoup(resp.text, "lxml")
        for entry in parse_reports(soup):
            if entry["regid"] == most_recent_id:
                stop = True
                break
            new_reports.append(entry)
        if stop:
            break

        next_url = get_next_page(soup)
        if next_url is None:
            break

    if not new_reports:
        return False
    cache.record_new_reports(new_reports)
    return True

def add_report_to_feed(feed, report, cfg):
    link = "http://{}/".format(report["host"])
    title = report["host"]
    pubdate = report["date"]

    tags = ["src-country:ru"]
    tags.extend("src-agency:" + html.escape(agency.lower())
                for agency in report["agencies"].split(","))

    # Because TagTeam currently doesn't support "enclosures", we
    # generate a description with links in it, instead.
    desc = "<h1>{} was reported as inaccessible</h1>".format(
        html.escape(report["host"]))

    desc += "<h2>Report logs</h2><ul>"
    desc += '<li><a href="{}">{}</a></li>'.format(
        urljoin(cfg.source, "/rec/{}".format(report["regid"])),
        report["regid"])
    desc += "</ul>"

    feed.add_item(title, link, desc, pubdate=pubdate, categories=tags,
                  unique_id=report["host"] + "|" + pubdate.isoformat())


def write_updated_feed(cfg, cache):
    feed = feedgenerator.Atom1Feed(
        title       = "Filtered Registry of Prohibited Sites (Russia)",
        link        = cfg.source,
        author_link = "https://roskomsvoboda.org/about-us/",
        description = ("Sites logged by the activist organization Роскомсвобода "
                       "as entered into the Russian Реестре запрещенных сайтов "
                       "(registry of prohibited sites).")
    )
    for report in cache.get_most_recent_reports():
        add_report_to_feed(feed, report, cfg)

    with open(cfg.dest_f, "wt") as fp:
        feed.write(fp, "utf-8")

def main():
    cfg = Config(sys.argv[1:])
    os.makedirs(os.path.dirname(cfg.dest_f), exist_ok=True)
    os.makedirs(os.path.dirname(cfg.cache_f), exist_ok=True)

    with Cache(cfg) as cache:
        if retrieve_new_reports(cfg, cache):
            write_updated_feed(cfg, cache)


main()
