#! /usr/bin/python3
#
# Copyright 2018 Zachary Weinberg.
# This program is free software; you may copy, modify, and distribute
# it under the terms of the GNU General Public License, version 3 (or,
# at your option, any later version).  See the file COPYING3 at the
# root of this source repository for exact license terms, or consult
# https://www.gnu.org/licenses/gpl.html

"""Translate Citizen Lab's lists of sensitive websites into an RSS
feed suitable for import to TagTeam.
"""

import collections
import configparser
import csv
import datetime
import html
import os
import subprocess
import sys
from   urllib.parse import urljoin, urlsplit

import feedgenerator

sys.path.insert(0, os.path.realpath(
    os.path.join(os.path.dirname(__file__), '../lib')))

import url

def parse_iso_date(date):
    y, m, d = date.split('-')
    return datetime.date(int(y), int(m), int(d))

def load_category_codes(catcode_f):
    with open(catcode_f) as fp:
        rd = csv.DictReader(fp)
        return {
            row['code'].strip().upper(): row['name'].strip().lower()
            for row in rd
            if row['code'] != '#'
        }

class Config:
    def __init__(self, extra_cfgs):
        parser = configparser.ConfigParser(
            strict=True,
            interpolation=None,
            default_section=None) # this only sorta disables DEFAULT
                                  # but it's the best we can do
        with open("input-feeds.cfg") as f:
            parser.read_file(f)
        parser.read(extra_cfgs)

        common = parser["common"]
        clab   = parser["citizen-lab"]

        self.source      = clab["source"]
        self.dest_f      = os.path.join(common["output_dir"], clab["dest"])
        self.checkout_d  = os.path.join(common["cache_dir"],  clab["checkout"])
        self.catcodes    = load_category_codes(common["category_codes"])

class URLRecord:
    EPOCH = datetime.date(1900, 1, 1)

    def __init__(self):
        self.countries  = set()
        self.categories = set()
        self.date_added = self.EPOCH

    def update(self, country, category, date_added):
        if country != 'global':
            self.countries.add(country)
        self.categories.add(category)
        self.date_added = max(self.date_added, date_added)


def update_git_checkout(cfg):
    """Clone and/or update the git repository holding the CitizenLab
       test lists."""

    if os.path.isdir(cfg.checkout_d):
        subprocess.run(["git", "pull", "--quiet"],
                       cwd=cfg.checkout_d, check=True)

    else:
        checkout_parent = os.path.dirname(cfg.checkout_d)
        os.makedirs(checkout_parent, exist_ok=True)
        subprocess.run(["git", "clone", "--quiet",
                        cfg.source, os.path.basename(cfg.checkout_d)],
                       cwd=checkout_parent, check=True)

def read_test_lists(cfg):
    """Read all of the test lists and return the relevant columns, as
       one big long iterator."""
    for e in os.scandir(os.path.join(cfg.checkout_d, "lists")):
        n = e.name
        if n.endswith(".csv") and not n.startswith("00-LEGEND-") and e.is_file():
            country_code = os.path.splitext(n)[0]
            with open(e.path, "rt") as fp:
                rd = csv.DictReader(fp)
                for row in rd:
                    yield (
                        url.canon_url_syntax(row['url'].strip()),
                        cfg.catcodes[row['category_code'].strip().upper()],
                        parse_iso_date(row['date_added'].strip()),
                        country_code
                    )

def collect_records(entries):
    """Collect the output of read_test_lists into per-URL records."""
    records = collections.defaultdict(URLRecord)
    for url, category, date_added, country_code in entries:
        records[url].update(country_code, category, date_added)

    return records

def add_record_to_feed(feed, url, record):
    tags = ["src-country:"+cc for cc in sorted(record.countries)]
    tags.extend("src-category:"+cat for cat in sorted(record.categories))
    feed.add_item(url, url, "",
                  pubdate=record.date_added, categories=tags,
                  unique_id=url+"|"+record.date_added.isoformat())

def write_feed(cfg, records):
    feed = feedgenerator.Atom1Feed(
        title       = "Citizen Lab URL testing lists",
        link        = cfg.source,
        author_link = "https://citizenlab.ca/",
        description = ("URLs thought to be more likely than average to be "
                       "subject to censorship, both globally and in specific "
                       "countries, curated by Citizen Lab.")
    )

    for url, record in reversed(sorted(
            records.items(),
            key = lambda kv: (kv[1].date_added, kv[0]))):
        add_record_to_feed(feed, url, record)

    with open(cfg.dest_f, "wt") as fp:
        feed.write(fp, "utf-8")

def main():
    cfg = Config(sys.argv[1:])
    os.makedirs(os.path.dirname(cfg.dest_f), exist_ok=True)

    update_git_checkout(cfg)
    write_feed(cfg, collect_records(read_test_lists(cfg)))

main()
