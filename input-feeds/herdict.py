#! /usr/bin/python3
#
# Copyright 2018 Zachary Weinberg.
# This program is free software; you may copy, modify, and distribute
# it under the terms of the GNU General Public License, version 3 (or,
# at your option, any later version).  See the file COPYING3 at the
# root of this source repository for exact license terms, or consult
# https://www.gnu.org/licenses/gpl.html

"""Read herdict.org's RSS feed of reports, filter them, and generate
another RSS feed suitable for import to TagTeam."""

import collections
import configparser
import csv
import datetime
import html
import os
import socket
import sqlite3
import sys

import requests
import feedparser
import feedgenerator

EPOCH = datetime.datetime.utcfromtimestamp(0)

def load_country_codes(cfg):
    with open(cfg.ccode_f) as fp:
        rd = csv.reader(fp)
        return { row[0].lower(): row[1].lower() for row in rd }

class Config:
    def __init__(self, extra_cfgs):
        parser = configparser.ConfigParser(
            strict=True,
            interpolation=None,
            default_section=None) # this only sorta disables DEFAULT
                                  # but it's the best we can do
        with open("input-feeds.cfg") as f:
            parser.read_file(f)
        parser.read(extra_cfgs)

        common  = parser["common"]
        herdict = parser["herdict"]

        self.source    = herdict["source"]
        self.max_hosts = int(herdict["max_hosts"])
        self.dest_f    = os.path.join(common["output_dir"], herdict["dest"])
        self.cache_f   = os.path.join(common["cache_dir"],  herdict["cache"])
        self.ccode_f   = common["country_codes"]
        self.country_code = load_country_codes(self)
        self.country_codes = sorted(self.country_code.values())

class Cache:
    """The persistent cache of Herdict reports.  Wraps a SQLite
       database.  No database queries should happen outside this
       class."""
    def __init__(self, cfg):
        self._conn = None
        self._cur  = None
        self._most_recent_reports = {}
        self._max_hosts = cfg.max_hosts
        self._country_codes = cfg.country_codes
        self._cache_f = cfg.cache_f

    def __enter__(self):
        self._conn = sqlite3.connect(self._cache_f,
                                    detect_types=sqlite3.PARSE_DECLTYPES,
                                    uri=False)
        self._conn.row_factory = sqlite3.Row
        self._cur = self._conn.cursor()

        # The cache database may or may not have just been created;
        # initialize it if necessary.
        # Unfortunately there's no "create table or, if it exists,
        # throw an error if it doesn't match this schema" option.
        with self._conn:
            cur = self._cur
            cur.executescript("""
                CREATE TABLE IF NOT EXISTS misc (
                    key   TEXT NOT NULL PRIMARY KEY,
                    value TEXT NOT NULL
                ) WITHOUT ROWID;

                -- This table has large rows, so WITHOUT ROWID is
                -- probably a lose.
                CREATE TABLE IF NOT EXISTS reports (
                    host   TEXT NOT NULL,
                    cc     TEXT NOT NULL,
                    date   TIMESTAMP NOT NULL,
                    ref    TEXT,
                    title  TEXT,
                    isp    TEXT,
                    comm   TEXT,
                    PRIMARY KEY (host, cc)
                );

                -- This index is used by get_most_recent_reports.
                CREATE INDEX IF NOT EXISTS reports_idx_cc_date ON reports
                  (cc, date DESC);
            """)

        return self

    def __exit__(self, *dontcare):
        self._cur.close()
        self._conn.close()
        return False

    def feed_last_modified(self):
        with self._conn:
            cur = self._cur
            cur.execute("SELECT value FROM misc WHERE key = 'Last-Modified';")
            rows = cur.fetchall()
            if not rows:
                return None
            assert len(rows) == 1 and len(rows[0]) == 1
            return rows[0][0]

    def set_feed_last_modified(self, last_modified):
        with self._conn:
            cur = self._cur
            cur.execute("""
                INSERT OR REPLACE INTO misc VALUES('Last-Modified', ?);
            """, (last_modified,))

    def most_recent_report(self, host, cc):
        key = host + "|" + cc
        if key not in self._most_recent_reports:
            with self._conn:
                cur = self._cur
                cur.execute("""
                    SELECT date FROM reports WHERE host = ? AND cc = ?;
                """, (host, cc))

                rows = cur.fetchall()
                if rows:
                    assert len(rows) == 1 and len(rows[0]) == 1
                    self._most_recent_reports[key] = rows[0][0]
                else:
                    self._most_recent_reports[key] = EPOCH

        return self._most_recent_reports[key]

    def record_new_reports(self, reports):
        html_esc = html.escape
        def escape_set_item(item):
            return html_esc(item or "").replace("|", "&#124;")
        def add_set_item(items, item):
            if not item: return items
            if not items: return item
            items = set(items.split("|"))
            items.add(item)
            return "|".join(sorted(items))

        cur = self._cur
        with self._conn:
            for report in reports:
                host  = report["host"]
                cc   = report["cc"]
                date  = report["date"]
                ref   = report["ref"]
                title = escape_set_item(report["title"])
                isp   = escape_set_item(report["isp"])
                comm  = escape_set_item(report["comm"])
                cur.execute("""
                    SELECT date, isp, title, comm
                      FROM reports
                     WHERE host = ? AND cc = ?
                """, (host, cc))
                rows = cur.fetchall()
                if not rows:
                    cur.execute("""
                        INSERT INTO reports VALUES(?, ?, ?, ?, ?, ?, ?);
                    """, (host, cc, date, ref, title, isp, comm))

                else:
                    assert len(rows) == 1 and len(rows[0]) == 4
                    row    = rows[0]
                    date   = max(row["date"], date)
                    isps   = add_set_item(row["isp"], isp)
                    titles = add_set_item(row["title"], title)
                    comms  = add_set_item(row["comm"], comm)

                    cur.execute("""
                        UPDATE reports
                           SET date = ?, ref = ?, isp = ?, title = ?, comm = ?
                         WHERE host = ? AND cc = ?;
                    """, (date, ref, isps, titles, comms, host, cc))

        # Possibly run ANALYZE here, rather than before closing
        # the connection (as the SQLite manual recommends), because
        # no further writes to the database will happen in this run,
        # and a bunch of complicated read queries are about to happen.
        cur.execute("PRAGMA optimize;")

    def get_most_recent_reports(self):
        # SQLite doesn't implement window functions, so we have to
        # fake it by issuing separate queries for each country code
        # and then merging the records.  Well, we would have to merge
        # the records anyway.
        merged_reports = collections.defaultdict(lambda: {
            "date":   EPOCH,
            "ccs":    set(),
            "refs":   set(),
            "isps":   set(),
            "titles": set(),
            "comms":  set(),
        })
        def merge_into_report(rec, date, cc, ref, title, isp, comm):
            rec["date"] = max(rec["date"], date)
            rec["ccs"].add(cc)
            rec["refs"].add(ref)
            rec["titles"].update(title.split("|"))
            rec["isps"].update(isp.split("|"))
            rec["comms"].update(comm.split("|"))

        with self._conn:
            cur = self._cur
            for cc in self._country_codes:
                cur.execute("""
                    SELECT host, date, ref, title, isp, comm
                      FROM reports
                     WHERE cc = ?
                  ORDER BY date DESC
                     LIMIT ?
                """, (cc, self._max_hosts))
                for host, date, ref, title, isp, comm in cur:
                    merge_into_report(merged_reports[host],
                                      date, cc, ref, title, isp, comm)

        sorted_reports = []
        for host, rec in merged_reports.items():
            rec["host"]   = host
            rec["ccs"].discard("")
            rec["isps"].discard("")
            rec["refs"].discard("")
            rec["titles"].discard("")
            rec["comms"].discard("")
            rec["ccs"]    = sorted(rec["ccs"])
            rec["refs"]   = sorted(rec["refs"])
            rec["isps"]   = sorted(rec["isps"])
            rec["titles"] = sorted(rec["titles"])
            rec["comms"]  = sorted(rec["comms"])
            sorted_reports.append(rec)
        sorted_reports.sort(key = lambda r: (r["date"], r["host"]))
        return sorted_reports

def retrieve_feed(cfg, cache):
    req_headers = {
        "User-Agent": "iclab-inputs/1.0 (zackw at cmu dot edu)"
    }
    lastmod = cache.feed_last_modified()
    if lastmod is not None:
        req_headers["If-Modified-Since"] = lastmod
    resp = requests.get(cfg.source, headers=req_headers)
    resp.raise_for_status()
    if resp.status_code == 304:
        return None
    if "Last-Modified" in resp.headers:
        cache.set_feed_last_modified(resp.headers["Last-Modified"])
    return resp.text

def is_ipaddr(val):
    try:
        socket.inet_pton(socket.AF_INET, val)
        return True
    except socket.error:
        try:
            socket.inet_pton(socket.AF_INET6, val)
            return True
        except socket.error:
            return False

def crunch_item(cfg, cache, item):
    # Skip items that were reported as accessible.
    if " accessible " in item.title:
        return None

    i_host    = ""
    i_title   = ""
    i_country = ""
    i_isp     = ""
    i_comment = ""
    for ln in item.description.splitlines():
        ln = ln.strip()
        if ln in ("", "<![CDATA[", "<br>", "<br/>", "<br />", "]]>"):
            continue
        k, _, v = ln.partition(":")
        k = k.rstrip().lower()
        v = v.lstrip()

        if k == "url":
            # actually a hostname
            # skip reports for bare IP addresses
            if is_ipaddr(v):
                return None

            i_host = v

        elif k == "title":
            i_title = v

        elif k == "reporter country":
            i_country = v

        elif k == "reporter isp":
            i_isp = v

        elif k == "comments":
            i_comment = v

        elif k == "report date":
            pass # we get the date more reliably from item.published_parsed

        else:
            sys.stderr.write("*** unrecognized key: {}\n".format(k))

    if not i_host or not i_country:
        return None

    i_cc = cfg.country_code[i_country.lower()]

    # Only the first 6 elements of the 9-tuple in published_parsed are
    # useful.
    i_date = datetime.datetime(*item.published_parsed[:6])

    if i_date <= cache.most_recent_report(i_host, i_cc):
        return None

    return {
        "host":  i_host,
        "cc":    i_cc,
        "title": i_title,
        "comm":  i_comment,
        "isp":   i_isp,
        "date":  i_date,
        "ref":   item.link,
    }

def add_report_to_feed(feed, report):
    link = "http://{}/".format(report["host"])
    title = report["titles"][0]
    pubdate = report["date"]

    tags = []
    tags.extend("src-country:"+cc for cc in report["ccs"])
    tags.extend("src-isp:"+html.escape(isp.lower())
                for isp in report["isps"])

    # Because TagTeam currently doesn't support "enclosures", we
    # generate a description with links in it, instead.
    desc = "<h1>{} was reported as inaccessible</h1>".format(
        html.escape(report["host"]))

    if len(report["titles"]) > 1:
        desc += "<h2>Alternative page titles</h2><ul>"
        for title in report["titles"][1:]:
            desc += "<li>{}</li>".format(html.escape(title))
        desc += "</ul>"

    if report["comms"]:
        desc += "<h2>Annotations</h2><ul>"
        for comm in report["comms"]:
            desc += "<li>{}</li>".format(html.escape(comm))
        desc += "</ul>"

    if report["refs"]:
        desc += "<h2>Report logs</h2><ul>"
        for ref in report["refs"]:
            desc += '<li><a href="{0}">{0}</a></li>'.format(html.escape(ref))
        desc += "</ul>"

    feed.add_item(title, link, desc, pubdate=pubdate, categories=tags,
                  unique_id=report["host"] + "|" + pubdate.isoformat())

def process(cfg, cache):
    feed = retrieve_feed(cfg, cache)
    if feed is None: return
    feed = feedparser.parse(feed)
    new_reports = []
    for item in feed.entries:
        crunched = crunch_item(cfg, cache, item)
        if crunched:
            new_reports.append(crunched)

    if not new_reports:
        return

    cache.record_new_reports(new_reports)

    o_feed = feedgenerator.Atom1Feed(
        title       = "Filtered " + feed.feed.title,
        link        = feed.feed.link,
        author_link = cfg.source,
        description = ("Sites reported to Herdict as inaccessible, "
                       "deduplicated, with tagging appropriate for "
                       "ICLab Inputs. ")
    )

    for report in cache.get_most_recent_reports():
        add_report_to_feed(o_feed, report)

    with open(cfg.dest_f, "wt") as fp:
        o_feed.write(fp, "utf-8")

def main():
    cfg = Config(sys.argv[1:])
    os.makedirs(os.path.dirname(cfg.dest_f), exist_ok=True)
    os.makedirs(os.path.dirname(cfg.cache_f), exist_ok=True)

    with Cache(cfg) as cache:
        process(cfg, cache)

main()
