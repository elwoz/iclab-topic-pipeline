#! /usr/bin/env node
'use strict';

const CDP           = require('chrome-remote-interface');
const program       = require('commander');

const ChromeDriver  = require('./lib/chromedriver');
const Output        = require('./lib/output');
const {createURLQueue} = require('./lib/url-queue');
const util          = require('./lib/util');

process.on('unhandledRejection', (err) => {
    console.error("*** unhandled rejection!");
    console.error(err);
    process.abort();
});

class UrlTask {
    constructor(driver, raw_url, url, label, options) {
        this.driver  = driver;
        this.raw_url = raw_url;
        this.url     = url;
        this.label   = label;
        this.sink    = options.output_sink;
        this.indent  = options.minified ? null : 4;
        this.future  = this.run();
        this.oname   = null;
        this.err     = null;
        this.startt  = Date.now();
        this.elapsed = null;
    }

    run() {
        return Promise.all([this.driver.capture_url(this.url.href),
                            this.sink.prepare(this.url.hostname)])
            .then(
                ([har, {stream, oname}]) => {
                    this.elapsed = Date.now() - this.startt;
                    this.oname = oname;
                    return new Promise((res, rej) => {
                        try {
                            stream.on('finish', () => res(this));
                            stream.on('error', (err) => {
                                this.error = err;
                                res(this);
                            });
                            stream.write(JSON.stringify(har, null,
                                                        this.indent));
                            stream.end('\n');
                        } catch (e) {
                            this.error = e;
                            res(this);
                        }
                    });
                },
                (err) => {
                    this.elapsed = Date.now() - this.startt;
                    this.err = err;
                    return this;
                }
            );
    }
}

async function process_urls(url_queue, idle_drivers, options) {
    // Process the URLs, one at a time per browser we have attached.
    let running = new Set();
    let todo = true;
    let sink = options.output_sink;
    let successes = 0, failures = 0, start = Date.now();

    let stop_flag = false;
    function on_signal (sig) {
        if (options.progress) {
            console.log(`SIGNAL ${sig}, shutting down...`);
        }
        stop_flag = true;
    }
    process.once('SIGINT', on_signal);
    process.once('SIGTERM', on_signal);

    while (running.size > 0 || (todo && idle_drivers.length > 0)) {
        while (!stop_flag && idle_drivers.length > 0) {
            let raw_url = await url_queue.get();
            if (!raw_url) {
                todo = false;
                break;
            }
            let url;
            try {
                url = util.canon_url_syntax(raw_url);
            } catch (e) {
                if (options.progress) {
                    console.log(`INVALID ${raw_url}`);
                }
                let oname = await sink.invalid_url_report(raw_url);
                await url_queue.done(raw_url, oname);
                // Invalid URLs count as successes because they do
                // not need to be retried later.
                successes++;
                continue;
            }
            if (url.protocol !== 'http:' && url.protocol !== 'https:') {
                if (options.progress) {
                    console.log(`INVALID ${url.href}`);
                }
                let oname = await sink.invalid_url_report(url);
                await url_queue.done(raw_url, oname);
                successes++;
                continue;
            }
            let driver = idle_drivers.pop();
            let label = `${driver._host}:${driver._port}`;
            if (options.progress) {
                console.log(`START ${label}: ${url.href}`);
            }
            running.add(new UrlTask(driver, raw_url, url, label, options));
        }
        if (options.progress) {
            console.log(`ZZZZ ${todo} ${running.size} ${idle_drivers.length}`);
        }
        if (!todo && running.size == 0) break;
        let first = await Promise.race(
            util.mapI(running.values(), t => t.future));
        if (options.progress) {
            console.log(`AAAA`);
        }
        running.delete(first);
        if (options.max_pages === 0
            || first.driver.pages_processed < options.max_pages) {
            idle_drivers.push(first.driver);
        }
        if (first.err !== null) {
            if (options.progress) {
                console.log(`FAIL ${first.label}: ${first.elapsed/1000}s ${first.url.href}`);
            }
            console.error(first.err);
            await url_queue.retry(first.raw_url);
            failures++;
            todo = true;
        } else {
            if (options.progress) {
                console.log(`DONE ${first.label}: ${first.elapsed/1000}s ${first.url.href}`);
            }
            await url_queue.done(first.raw_url, first.oname);
            successes++;
        }
        if (options.progress) {
            console.log(`AAAA ${todo} ${running.size} ${idle_drivers.length}`);
        }
    }
    if (options.progress) {
        let stop = Date.now();
        let elapsed = (start - stop)/1000;
        console.log(`COMPLETE ${successes}, to-retry ${failures} in ${elapsed}s`);
    }
}

async function run(options) {
    options.output_sink = new Output({
        dir:      options.output_dir,
        s3:       options.output_s3,
        s3cred:   options.s3_creds,
        compress: options.compress,
        progress: options.progress
    });

    let drivers = [];
    for (let {host, port} of util.flatten(await Promise.all(options.browser))) {
        for (let i = 0; i < options.tabs; i++) {
            drivers.push(new ChromeDriver(host, port, options));
        }
    }
    // my kingdom for actual context managers
    let url_queue = await createURLQueue(options);
    try {
        await process_urls(url_queue, drivers, options);
    } finally {
        await url_queue.close();
    }
}

function main() {
    program
        .usage('[options] URL...')
        .option('-b, --browser <host:port>',
                'network address of browser to drive (default 127.0.0.1:9222)'+
                ' (may be repeated)', util.collect_net_addrs)
        .option('-o, --output-dir <dir>', 'Directory to write HARs to')
        .option('-S, --output-s3 <url>',
                'S3-compatible storage URL to write HARs to')
        .option('--s3-creds <file>',
                'File containing S3 access credentials: access key and '+
                'secret key, one line each')
        .option('--redis <host:port>',
                'network address of Redis url queue (default 127.0.0.1:6379)',
                util.parse_net_addr)
        .option('--redis-retry-timeout <time>',
                'total time to wait before giving up on Redis '+
                '(default 30min)',
                util.parse_timeout)
        .option('--redis-retry-limit <n>',
                'maximum number of retries to make before giving up on Redis '+
                '(default 100)',
                util.parse_nonneg_int)
        .option('-m, --minified', 'Minify (do not pretty-print) the HARs')
        .option('-z, --compress', 'Compress the HARs using gzip')
        .option('-p, --progress', 'Report progress to console')
        .option('--width <dip>', 'frame width in DIP', util.parse_nonneg_int)
        .option('--height <dip>', 'frame height in DIP', util.parse_nonneg_int)
        .option('--load-images', 'Load images and other media')
        .option('--capture-screen', 'Record a screenshot after loading')
        .option('--capture-dom', 'Record the DOM tree after loading')
        .option('--tabs <n>',
                'Number of pages to load simultaneously per browser',
                util.parse_nonneg_int)
        .option('--max-pages <n>',
                'Maximum pages to process per browser (0 = unlimited)',
                util.parse_nonneg_int)
        .option('-t, --timeout <time>',
                'time to wait before giving up on a URL (default: 5m)',
                util.parse_timeout)
        .parse(process.argv);

    if (program.redis !== undefined && program.args.length > 0) {
        console.error("use either --redis or an URL list, not both");
        program.outputHelp();
        process.exit(1);
    }
    if (program.redis === undefined && program.args.length === 0) {
        console.error("must supply either --redis or an URL list");
        program.outputHelp();
        process.exit(1);
    }

    const defaults = {
        browser: [Promise.resolve({host: "127.0.0.1", port: 9222})],
        timeout: 5 * 60 * 1000,
        redis: Promise.resolve([{host: "127.0.0.1", port: 6379}]),
        redis_retry_timeout: 30 * 60 * 1000,
        redis_retry_limit: 100,
        minified: false,
        compress: false,
        progress: false,
        width: 1024,
        height: 768,
        load_images: false,
        capture_screen: false,
        capture_dom: false,
        max_pages: 500,
        tabs: 1
    };

    run(util.extract_options(program, defaults))
        .then(() => { process.exit(0); })
        .catch(err => {
            console.error(err);
            process.exit(1);
        });
}
main();

// Local Variables:
// mode: js2
// js2-skip-preprocessor-directives: t
// js2-include-node-externs: t
// End:
