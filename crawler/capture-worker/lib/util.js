/// Miscellaneous functions.
'use strict';

const dns = require('dns');
const URL = require('url').URL;

/// Map over an iterator.
exports.mapI = function* mapI(iterator, mapping) {
    while (true) {
        let result = iterator.next();
        if (result.done) {
            break;
        }
        yield mapping(result.value);
    }
};

/// Flatten an arbitrarily-nested array of (arrays of ...) arrays.
exports.flatten = function flatten(arr, result = []) {
    for (let i = 0, length = arr.length; i < length; i++) {
        const value = arr[i];
        if (Array.isArray(value)) {
            flatten(value, result);
        } else {
            result.push(value);
        }
    }
    return result;
};

/// Syntactically canonicalize a URL.  This makes the following
/// transformations:
///      - a bare hostname becomes "http://${hostname}/"
///      - scheme and hostname are lowercased
///      - hostname is punycoded if necessary
///      - vacuous user, password, and port fields are stripped
///      - ports redundant to the scheme are also stripped
///      - path becomes '/' if empty
///      - characters outside the printable ASCII range in path,
///        query, fragment, user, and password are %-encoded, as are
///        improperly used % signs
///
/// Note: returns a URL object, not a string.
exports.canon_url_syntax = function canon_url_syntax(xurl) {
    try {
        // The URL object does all of the canonicalizations of valid, absolute
        // urls that we want.
        return new URL(xurl);
    } catch (e) {}

    // Heuristics for fixing up invalid URLs.
    // Common typos involving the scheme:
    let fixed;
    fixed = xurl.replace(/^(?::+\/*|\/+)/, "http://");
    if (xurl !== fixed) {
        return new URL(fixed);
    }

    fixed = xurl.replace(/^(https?|ftps?)(?::+\/*|\/+)/i, "$1://");
    if (xurl !== fixed) {
        return new URL(fixed);
    }

    fixed = xurl.replace(/^(?:htp|ttp|hp)(s?)(?::+\/*|\/+)/i, "http$1://");
    if (xurl !== fixed) {
        return new URL(fixed);
    }

    // Failing that, assume what we have here is a bare hostname.
    return new URL("http://" + xurl);
};

/// Convert from a commander 'program' object to a plain dictionary of options,
/// also mapping from commander's camelCased names for --multi-word-options
/// to properly snake_cased names.
exports.extract_options = function extract_options(program, defaults) {
    function camel_name(opt) {
        return opt.long.replace('--', '').replace('no-', '')
            .split('-').reduce(function(str, word) {
                return str + word[0].toUpperCase() + word.slice(1);
            });
    }
    function snake_name(opt) {
        return opt.long.replace('--', '').replace('no-', '').replace('-', '_');
    }
    let options = {};
    for (let opt of program.options) {
        let commander_name = camel_name(opt);
        let desired_name   = snake_name(opt);
        if (program.hasOwnProperty(commander_name)) {
            options[desired_name] = program[commander_name];
        }
    }
    for (let opt of Object.keys(defaults)) {
        if (!options.hasOwnProperty(opt)) {
            options[opt] = defaults[opt];
        }
    }
    options.args = program.args;
    return options;
};

/// Parse a timeout from the command line.
/// Timeouts can be specified in any of these formats:
///   1.23   (unit defaults to seconds)
///   1.23s | 1.23sec  (explicit seconds)
///   1.23m | 1.23min  (minutes)
///   1.23ms           (milliseconds)
///   2:1.23 (minutes and seconds)
///   2m1.23s (minutes and seconds)
///
/// Returns a number of milliseconds, or throws an error.
exports.parse_timeout = function parse_timeout(opt) {
    function invalid(opt) {
        return new Error("invalid timeout: '" + opt + "'");
    }

    let m = /^(\.[0-9]+|[0-9]+\.?[0-9]*)(|:|s(?:ec)?|m(?:s|in)?)((?:\.[0-9]+|[0-9]+\.?[0-9]*)?)(s?)$/i
        .exec(opt);
    if (m === null) throw invalid(opt);

    let [, n1, s1, n2, s2] = m;
    n1 = parseFloat(n1);
    if (!isFinite(n1) || n1 <= 0) throw invalid(opt);

    if (s1 === 'ms') {
        if (n2 !== '' || s2 !== '') throw invalid(opt);
        return n1;
    }
    if (s1 === '') {
        // if s1 and n2 are both empty, s2 might have matched an 's',
        // which is fine
        if (n2 !== '') throw invalid(opt);
        return n1 * 1000;
    }
    if (s1 === 's' || s1 === 'sec') {
        if (n2 !== '' || s2 !== '') throw invalid(opt);
        return n1 * 1000;
    }
    if (s1 === 'min') {
        if (n2 !== '' || s2 !== '') throw invalid(opt);
        return n1 * 60 * 1000;
    }
    if (s1 === 'm') {
        if (n2 === '') {
            // Despite the maximal-munch principle, for some damn reason
            // the regex engine may split 'ms' across s1 and s2.
            if (s2 === 's') {
                return n1;
            }
            // otherwise, s2 must be ''
            n2 = 0;
        } else {
            // in this case s2 can be either '' or 's' and it doesn't matter
            // since seconds were specified, minutes must be whole
            n2 = parseFloat(n2);
            if (!isFinite(n2) || n2 < 0 || n2 >= 60) throw invalid(opt);
            if (n1 !== Math.floor(n1)) throw invalid(opt);
        }
        return (n1 * 60 + n2) * 1000;
    }
    if (s1 === ':') {
        // with a colon, following seconds are mandatory and an 's' suffix
        // is forbidden
        if (n2 === '' || s2 !== '') throw invalid(opt);
        if (n1 !== Math.floor(n1)) throw invalid(opt);
        n2 = parseFloat(n2);
        if (!isFinite(n2) || n2 < 0 || n2 >= 60) throw invalid(opt);
        return (n1 * 60 + n2) * 1000;
    }
    throw invalid(opt);
};

/// Parse a single host:port network address.
/// Acceptable forms are:
///   - just a port number (implicit 127.0.0.1 for host)
///   - host.name.example:port, 198.51.0.1:port, [2001:db8::0001]:port
/// Returns a Promise which will resolve to an object of the form
///     { host: "198.51.0.1", port: 2345 }
/// after getaddrinfo is done.
function parse_net_addr(opt) {
    function invalid(opt) {
        return new Error("invalid network address: '" + opt + "'");
    }
    let m = /^(?:(\[[0-9a-fA-F:]+\]|[0-9a-zA-Z.-]+):)?([0-9]+)$/.exec(opt);
    if (m === null)
        throw invalid(opt);

    let [, host, port] = m;
    port = parseInt(port);
    if (port <= 0 || port >= 65536)
        throw invalid(opt);

    if (host === undefined) {
        return Promise.resolve([{ host: "127.0.0.1", port: port }]);
    }

    // Remove square brackets around IPv6 address literals.
    // We don't try to discriminate address literals from hostnames,
    // we just pass them all down and let getaddrinfo sort them out.
    if (host[0] === '[') {
        host = host.slice(1, -1);
    }
    return new Promise((succ, fail) => {
        dns.lookup(host, { all: true, verbatim: true }, (err, addrs) => {
            if (err)
                fail(err);
            else
                succ(addrs.map((a) => ({ host: a.address, port: port })));
        });
    });
}
exports.parse_net_addr = parse_net_addr;

/// Collect one or more host:port network addresses into an array.
exports.collect_net_addrs = function collect_net_addrs(opt, memo) {
    if (!memo) memo = [];
    memo.push(parse_net_addr(opt));
    return memo;
};

/// Parse a command-line option that must be a non-negative integer.
exports.parse_nonneg_int = function parse_nonneg_int(opt) {
    function invalid(opt) {
        return new Error(`not a nonnegative integer: '${opt}'`);
    }
    if (!/^[0-9]+$/.test(opt))
        throw invalid(opt);
    let val = parseInt(opt, 10);
    if (Number.isNaN(val))
        throw invalid(opt);
    return val;
}

// Local Variables:
// mode: js2
// js2-skip-preprocessor-directives: t
// js2-include-node-externs: t
// End:
