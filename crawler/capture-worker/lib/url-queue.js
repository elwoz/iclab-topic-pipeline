/// The URL queue tells the worker which URLs to load.
'use strict';

const redis = require("redis");
const {promisify} = require("util");

class LocalURLQueue {
    constructor(urls) {
        this.urls = urls;
    }

    async get() {
        return this.urls.pop();
    }

    async done(url, oname) {
        console.log(`${oname}: ${url}`);
    }

    async retry(url) {
        this.urls.push(url);
    }

    async close() {
    }
}

class RedisURLQueue {
    constructor(host, port, retry_limit, retry_timeout) {
        this.client = redis.createClient({
            host, port,
            prefix: "url_queue:",
            retry_strategy: function (status) {
                // Exponential backoff starting at 1 second going up to
                // whatever retry_timeout is; no more than retry_limit
                // iterations.  Log the errors.  Note that retry_timeout
                // is in seconds, but the total_retry_time and the value
                // returned are in milliseconds.
                console.log(
                    `redis: ${host}:${port}: network error: ${status.error}`);
                if (status.attempt > retry_limit ||
                    status.total_retry_time >= retry_timeout * 1000) {
                    return status.error;
                }
                let retry = Math.max(100, Math.min(
                    Math.pow(2, status.attempt - 1) * 1000,
                    (retry_timeout * 1000) - status.total_retry_time));
                console.log(`redis: will retry after ${retry / 1000} s`);
                return retry;
            }
        });
        this.error_future = new Promise((s, f) => {
            this.client.on("error", (e) => f(e));
            this.no_more_errors = s;
        });
        this.setup_future = this._async_setup();
        this.closed = false;
        this.should_stop = false;
    }

    _redis(op, ...args) {
        return Promise.race([
            promisify(this.client[op]).call(this.client, ...args),
            this.error_future
        ]);
    }

    _flush_pending() {
        // If there are any URLs in this worker's processing list,
        // flush them back to the unprocessed list, in one atomic operation.
        return this._redis("EVAL",
                           "local e = redis.call('LRANGE', KEYS[2], 0, -1)\n"+
                           "if next(e) ~= nil then\n"+
                           "  redis.call('LPUSH', KEYS[1], unpack(e))\n"+
                           "  redis.call('DEL', KEYS[2])\n"+
                           "end", 2, "unprocessed", this.k_processing);
    }

    async _async_setup() {
        let r, k_lastop;
        let id = 0;
        let startup_time = Date.now();
        // Claim the first worker ID that isn't already in use.
        for (;;) {
            k_lastop = `w${id}:lastop`;
            r = await this._redis("SET", k_lastop, startup_time, "NX");
            if (r === "OK") break;
            id++;
        }
        this.k_lastop     = k_lastop;
        this.k_processing = `w${id}:processing`;

        await this._redis("CLIENT", "SETNAME", `url_worker:${id}`);
        await this._flush_pending();
        await this._redis("SET", k_lastop, Date.now());
    }

    async get() {
        if (this.closed) throw new Error("get called on closed queue");
        if (this.should_stop) {
            console.log(`redis: get() after should_stop`);
            return undefined;
        }
        await this.setup_future;

        let r = await this._redis(
            "BRPOPLPUSH", "unprocessed", this.k_processing, 0);
        if (r === "stop://") {
            await this._redis("LREM", this.k_processing, 1, r);
            this.should_stop = true;
            r = undefined;
        }
        await this._redis("SET", this.k_lastop, Date.now());
        return r;
    }

    async done(url, oname) {
        if (this.closed) throw new Error("done called on closed queue");
        if (url === undefined)
            throw new Error("done called with end-of-queue sentinel");
        await this.setup_future;
        await this._redis("EVAL",
                          "redis.call('LREM', KEYS[1], 1, ARGV[1]);"+
                          "return redis.call('HSET', KEYS[2], ARGV[1], ARGV[2]);",
                          2, this.k_processing, "processed", url, oname);
    }

    async retry(url) {
        if (this.closed) throw new Error("retry called on closed queue");
        if (url === undefined)
            throw new Error("retry called with end-of-queue sentinel");
        await this.setup_future;
        await this._redis("EVAL",
                          "redis.call('LREM', KEYS[1], 1, ARGV[1]);"+
                          "return redis.call('LPUSH', KEYS[2], ARGV[1]);",
                          2, this.k_processing, "unprocessed", url);
    }

    async close() {
        if (this.closed) return;
        this.closed = true;
        await this.setup_future;
        await this._flush_pending();
        await this._redis("DEL", this.k_lastop);
        await this._redis("QUIT");
        this.no_more_errors();
        await this.error_future;
    }
}

async function createURLQueue(options) {
    if (options.args.length > 0) {
        return new LocalURLQueue(options.args);
    } else {
        let redis = (await options.redis)[0];
        if (options.progress) {
            console.log(`REDIS connecting to ${redis.host}:${redis.port}`);
        }
        return new RedisURLQueue(redis.host,
                                 redis.port,
                                 options.redis_retry_limit,
                                 options.redis_retry_timeout);
    }
}

module.exports = { LocalURLQueue, RedisURLQueue, createURLQueue };

// Local Variables:
// mode: js2
// js2-skip-preprocessor-directives: t
// js2-include-node-externs: t
// End:
