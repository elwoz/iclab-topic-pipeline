/// The code in this file was adopted from chrome-har-capturer
/// <https://github.com/cyrus-and/chrome-har-capturer>
/// with extensive modifications.
"use strict";

const CDP = require("chrome-remote-interface");
const HAR = require("./har");

const BROWSER_TARGET = "/devtools/browser";
const BLANK_URL = "about:blank";
class ChromeContext {
    constructor(host, port, options) {
        this._host = host;
        this._port = port;
        this._cleanup = [];
        this._options = options;
    }

    async create() {
        // fetch the browser version (since Chrome 62 the browser target URL is
        // generated at runtime and can be obtained via the "/json/version"
        // endpoint, fallback to "/devtools/browser" if not present)
        const {webSocketDebuggerUrl} = await CDP.Version({
            host: this._host, port: this._port
        });
        // connect to the browser target
        const browser = await CDP({
            host: this._host,
            port: this._port,
            target: webSocketDebuggerUrl || BROWSER_TARGET
        });
        this._cleanup.unshift(async () => {
            await browser.close();
        });
        // request a new browser context
        const {Target} = browser;
        const {browserContextId} = await Target.createBrowserContext();
        this._cleanup.unshift(async () => {
            await Target.disposeBrowserContext({browserContextId});
        });
        // create a new empty tab
        const {width, height} = this._options;
        const {targetId} = await Target.createTarget({
            url: BLANK_URL,
            width, height,
            browserContextId
        });
        this._cleanup.unshift(async () => {
            await Target.closeTarget({targetId});
        });
        // connect to the tab and return the handler
        const tab = await CDP({
            host: this._host,
            port: this._port,
            target: targetId
        });
        this._cleanup.unshift(async () => {
            await tab.close();
        });
        return tab;
    }

    async destroy() {
        // run cleanup handlers
        for (const handler of this._cleanup) {
            await handler();
        }
    }
}

class Stats {
    constructor(url, options) {
        this._options = options;
        this._responseBodyPending = 0;
        this._requestsIssued = 0;
        this._requestsIssuedLast = undefined;
        this._beginTime = undefined;
        this._timeout = options.timeout || (5 * 60 * 1000);
        this._ccInterval = this._timeout / 30;
        this._completionChecker = setInterval(
            this._completionCheckTimer.bind(this),
            this._ccInterval);
        this._onCompletion = undefined;
        this._onAfterLoad = undefined;
        this._navComplete = false;
        this._afterLoadHookComplete = true;
        this._hasScripts = false;

        this.url = url;
        this.firstRequestId = undefined;
        this.firstRequestMs = undefined;
        this.domContentEventFiredMs = undefined;
        this.loadEventFiredMs = undefined;
        this.entries = new Map();
        this.user = undefined; // filled from outside
    }

    // Note: we only implement once() and not on(), and only  one callback
    // for each event, because that's all ChromeDriver needs.
    once(what, cb) {
        if (what === "completion") {
            this._onCompletion = cb;
        } else if (what === "afterload") {
            this._onAfterLoad = cb;
            this._afterLoadHookComplete = false;
        } else {
            throw new Error("invalid Stats event '" + what + "'");
        }
    }

    processEvent({method, params}) {
        const methodName = `_${method.replace(".", "_")}`;
        const handler = Stats.prototype[methodName];
        if (handler) {
            handler.call(this, params);
        }
    }

    begin(navigation) {
        this._beginTime = Date.now();
        navigation.then(() => { this._navComplete = true; });
    }

    end() {
        clearInterval(this._completionChecker);
    }

    /// This function is called every _ccInterval milliseconds by a
    /// timer.  It is also called by certain event handlers below.
    /// Its purpose is to decide whether we are done loading the page.
    ///
    /// When the first request has received a reply, the
    /// Page.domContentEventFired and Page.loadEventFired events have
    /// occurred, and we are not waiting for any additional response
    /// bodies to show up, the page _could_ be finished.  If there
    /// are no scripts in the current page, then we _are_ finished.
    /// Otherwise, we wait until one full _ccInterval has elapsed
    /// without any new network requests having been issued.
    ///
    /// In any case, we give up after the complete _timeout interval has
    /// expired since page loading began.

    _completionCheck() {
        if (this._onCompletion === undefined)
            return;

        if ((this._initialLoadComplete()
             && (!this._hasScripts
                 || this._requestsIssued === this._requestsIssuedLast))
            || (Date.now() - this._beginTime) >= this._timeout)
        {
            this._onCompletion();
            this._onCompletion = undefined;
        }
    }

    _completionCheckTimer() {
        this._completionCheck();
        this._requestsIssuedLast = this._requestsIssued;
    }

    _maybeAfterLoadHook() {
        if (this.domContentEventFiredMs && this.loadEventFiredMs) {
            if (this._onAfterLoad !== undefined) {
                this._onAfterLoad(this).then(() => {
                    this._afterLoadHookComplete = true;
                    this._completionCheck();
                });
                this._onAfterLoad = undefined;
            } else {
                this._completionCheck();
            }
        }
    }

    _initialLoadComplete() {
        if (!this._navComplete ||
            !this._afterLoadHookComplete ||
            !this._responseBodyPending ||
            !this.domContentEventFiredMs ||
            !this.loadEventFiredMs ||
            this.firstRequestId === undefined) {
            return false;
        }
        let entry = this.entries.get(this.firstRequestId);
        if (!entry) {
            return false;
        }
        if (entry.responseFinishedS === undefined) {
            return false;
        }
        return true;
    }

    //
    // Handle various events coming in from Chrome.
    //

    _Debugger_scriptParsed(params) {
        this._hasScripts = true;
    }

    _Page_domContentEventFired(params) {
        const {timestamp} = params;
        this.domContentEventFiredMs = timestamp * 1000;
        this._maybeAfterLoadHook();
    }

    _Page_loadEventFired(params) {
        const {timestamp} = params;
        this.loadEventFiredMs = timestamp * 1000;
        this._maybeAfterLoadHook();
    }

    _Network_requestWillBeSent(params) {
        const {requestId, initiator, timestamp, redirectResponse} = params;
        // skip data URIs
        if (/^data:/.test(params.request.url)) {
            return;
        }
        // the first is the first request
        if (!this.firstRequestId && initiator.type === "other") {
            this.firstRequestMs = timestamp * 1000;
            this.firstRequestId = requestId;
        }
        // redirect responses are delivered along the next request
        if (redirectResponse) {
            const redirectEntry = this.entries.get(requestId);
            // craft a synthetic response params
            redirectEntry.responseParams = {
                response: redirectResponse
            };
            // set the redirect response finished when the redirect
            // request *will be sent* (this may be an approximation)
            redirectEntry.responseFinishedS = timestamp;
            redirectEntry.encodedResponseLength =
                redirectResponse.encodedDataLength;
            // since Chrome uses the same request id for all the
            // redirect requests, it is necessary to disambiguate
            const newId = requestId + "_redirect_" + timestamp;
            // rename the previous metadata entry
            this.entries.set(newId, redirectEntry);
            this.entries.delete(requestId);
        }
        // initialize this entry
        this.entries.set(requestId, {
            requestParams: params,
            responseParams: {},
            responseLength: 0, // built incrementally
            encodedResponseLength: undefined,
            responseFinishedS: undefined,
            responseBody: undefined,
            responseBodyIsBase64: undefined,
            newPriority: undefined
        });
        this._requestsIssued++;
    }

    _Network_dataReceived(params) {
        const {requestId, dataLength} = params;
        const entry = this.entries.get(requestId);
        if (!entry) {
            return;
        }
        entry.responseLength += dataLength;
    }

    _Network_responseReceived(params) {
        const entry = this.entries.get(params.requestId);
        if (!entry) {
            return;
        }
        entry.responseParams = params;
    }

    _Network_resourceChangedPriority(params) {
        const {requestId, newPriority} = params;
        const entry = this.entries.get(requestId);
        if (!entry) {
            return;
        }
        entry.newPriority = newPriority;
    }

    async _Network_loadingFinished(params) {
        const {requestId, timestamp, encodedDataLength} = params;
        const entry = this.entries.get(requestId);
        if (!entry) {
            return;
        }
        entry.encodedResponseLength = encodedDataLength;
        entry.responseFinishedS = timestamp;
        // the body will be delivered in a separate callback
        this._responseBodyPending++;
    }

    _Network_loadingFailed(params) {
        const {requestId, errorText, canceled, timestamp} = params;
        const entry = this.entries.get(requestId);
        if (!entry) {
            return;
        }
        entry.responseFinishedS = timestamp;
        entry.responseParams = {
            response: {
                url: "",
                status: undefined,
                statusText: errorText,
                headers: {},
                mimeType: undefined,
                connectionReused: false,
                connectionId: undefined,
                encodedDataLength: 0,
                securityState: "unknown",
                timing: {
                    requestTime: timestamp
                }
            }
        };
        if (requestId === this.firstRequestId) {
            // if the first request fails, the load events will not happen,
            // so, for purpose of _completionCheck, pretend they occurred now
            this.domContentEventFiredMs = timestamp * 1000;
            this.loadEventFiredMs = timestamp * 1000;
        }
    }

    _Network_getResponseBody(params) {
        const {requestId, body, base64Encoded, err} = params;
        const entry = this.entries.get(requestId);
        if (!entry) {
            return;
        }
        entry.responseBody = body;
        entry.responseBodyIsBase64 = base64Encoded;
        // FIXME record error if any

        this._responseBodyPending--;
        if (this._responseBodyPending == 0)
            this._completionCheck();
    }
}

class ChromeDriver {
    constructor(host, port, options) {
        this._host = host;
        this._port = port;
        this._options = options;
        this.pages_processed = 0;
    }

    async capture_url(url) {
        this.pages_processed += 1;
        return HAR.create(await this._load_page(url));
    }

    async _load_page(url) {
        // create a fresh new context for this URL
        const context = new ChromeContext(this._host, this._port,
                                          this._options);
        // my kingdom for actual context managers
        try {
            const stats = new Stats(url, this._options);
            try {
                const client = await context.create();
                return await this._load_page_with_client(client, stats, url);
            } finally {
                await stats.end();
            }
        } finally {
            await context.destroy();
        }
    }

    async _load_page_with_client(client, stats, url) {
        // enable domains
        const {Browser, Debugger, DOM, Page, Network} = client;
        await Debugger.enable();
        await Network.enable();
        await Page.enable();
        await DOM.enable();

        // Adjust the user agent:
        let ua = (await Browser.getVersion()).userAgent;
        // Do not reveal that we are "headless".
        ua = ua.replace(" HeadlessChrome/", " Chrome/");
        // Pretend to be on Windows.  I wish Chrome didn't casually
        // reveal this much information about the operating system.
        // Actually I wish it didn't reveal the OS at all.
        ua = ua.replace(/^Mozilla\/5.0 \([^)]+\) /,
                        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) ");

        await Network.setUserAgentOverride({userAgent: ua});

        // By default, don't load images, fonts, audio or video.
        // TODO: Consider also blocking ads, which would require
        // downloading and evaluating filter lists.
        if (!this._options.load_images) {
            await Network.setRequestInterception({
                patterns: [
                    {resourceType: "Image"},
                    {resourceType: "Font"},
                    {resourceType: "Media"},
                ]
            });

            Network.requestIntercepted((args) => {
                // This can fail due to a number of race conditions;
                // we don't care, but we must explicitly swallow
                // errors or the entire process will crash on an
                // unhandled rejection.
                Network.continueInterceptedRequest({
                    interceptionId: args.interceptionId,
                    errorReason: "Aborted"
                }).then(() => null, (e) => null);
            });
        }

        // Register events.
        stats.once("afterload",
                   this._after_load_hook.bind(this, client));
        client.on("event", stats.processEvent.bind(stats));
        Network.loadingFinished(async ({requestId}) => {
            let params;
            try {
                params = await Network.getResponseBody({requestId});
            } catch (err) {
                params = { body: "", base64Encoded: false, err: err };
            }
            let {body, base64Encoded, err} = params;
            stats.processEvent({
                method: "Network.getResponseBody",
                params: {
                    requestId,
                    body,
                    base64Encoded,
                    err
                }
            });
        });

        // start the page load and wait for it to finish
        await new Promise((fulfill, reject) => {
            stats.once("completion", fulfill);
            client.once("disconnect", reject);
            client.once("error", reject);
            stats.begin(Page.navigate({url}));
        });

        // optionally record the HTML and/or screen after any scripted
        // DOM updates; these can fail if the page load was sufficiently
        // screwed up
        stats.user = {};
        if (this._options.capture_dom) {
            try {
                let doc = await DOM.getDocument({});
                let outerHTML = await DOM.getOuterHTML(
                    {nodeId: doc.root.nodeId});
                stats.user.dom_capture = outerHTML.outerHTML;
            } catch (e) {
                stats.user.dom_capture_error = e;
            }
        }
        if (this._options.capture_screen) {
            try {
                stats.user.screen_capture =
                    (await Page.captureScreenshot({})).data;
            } catch (e) {
                stats.user.screen_capture_error = e;
            }
        }
        return stats;
    }

    async _after_load_hook(client, stats) {
        if (!stats._hasScripts) return;

        // Borrowed from OpenWPM: randomly scroll down a few times.
        // TODO: Wave the mouse around randomly as well? But don't click.
        let Input = client.Input;
        let cycle = 0;
        while (cycle < 10) {
            await Input.synthesizeScrollGesture({
                x: 123 + Math.random() * 20,
                y: 40 + Math.random() * 20,
                xDistance: 0,
                yDistance: -800 + Math.random() * 40
            });
            await new Promise((r)=>{setTimeout(r, (.5+Math.random())*1000);});
            cycle++;
            if (Math.random() > (cycle / 10.)) break;
        }
    }
}

module.exports = ChromeDriver;

// Local Variables:
// mode: js2
// js2-include-node-externs: t
// End:
