/// Various output sinks for the HARs recorded by this program.
"use strict";

const fs      = require("fs");
const path    = require("path");
const stream  = require("stream");
const URL     = require('url').URL;
const util    = require("util");
const zlib    = require("zlib");

const mkdirp  = require("mkdirp");
const minio   = require("minio");

const fs_open = util.promisify(fs.open);

//
// disk storage
//
async function disk_store(base, dir, gzip) {
    base = path.join(dir, base);
    let i = 1;
    let fd;
    let oname;
    for (;;) {
        oname = `${base}-${i}.har${gzip ? ".gz" : ""}`;
        try {
            fd = await fs_open(oname, "wx");
            break;
        } catch (err) {
            if (err.code !== "EEXIST")
                throw err;
            i += 1;
            if (i >= 10000)
                throw err;
        }
    }
    let stream = fs.createWriteStream(oname, { fd });
    if (gzip) {
        let zstream = zlib.createGzip({ level: 9 });
        zstream.pipe(stream);
        // piping doesn't automatically close the sink on error;
        // if we don't, no one will
        zstream.on('error', () => stream.end());
        stream = zstream;
    }
    return { stream, oname };
}

//
// minio storage
//
function load_credential_file(fname) {
    // A credential file just has the access key and secret key on
    // its first two lines.
    return fs.readFileSync(fname, { encoding: "utf8" })
        .trim().split(/\s+/).slice(0, 2);
}

function minio_client_from_options(options, bucket) {
    let url = new URL(options.s3);
    let endPoint = url.hostname;
    if (url.protocol !== "https:" && url.protocol !== "http:") {
        throw new Error("Minio does not support access via " +
                        url.proto.slice(0,-1));
    }
    let secure = (url.protocol === "https:");
    let port = url.port ? parseInt(url.port) : secure ? 443 : 80;
    let [accessKey, secretKey] = load_credential_file(options.s3cred);
    let progress = options.progress;
    let client = new minio.Client({
        endPoint, port, secure, accessKey, secretKey });

    // if we create the bucket, also set it to be world-readable
    let bucketPolicy = JSON.stringify({
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid":       "AddPerm",
                "Effect":    "Allow",
                "Principal": {"AWS":["*"]},
                "Action":    ["s3:ListBucket"],
                "Resource":  [`arn:aws:s3:::${bucket}`]
            },
            {
                "Sid":       "AddPerm",
                "Effect":    "Allow",
                "Principal": {"AWS":["*"]},
                "Action":    ["s3:GetObject"],
                "Resource":  [`arn:aws:s3:::${bucket}/*`]
            }
        ]
    });

    let bucketFuture =
        client.makeBucket(bucket)
        .then(() => client.setBucketPolicy(bucket, bucketPolicy))
        .catch((e) => {
            // it's ok if the bucket already exists; we assume it's
            // not necessary to reset the bucket policy in that case
            if (e.code !== 'BucketAlreadyOwnedByYou')
                throw e;
        });

    if (progress) {
        console.log(`MINI connecting to ${endPoint}:${port}/${bucket}`);
    }
    return { client,
             bucket,
             bucketFuture,
             bucketPending: true,
             progress,
             base_url: `${url.href}${bucket}/`
           };
}


async function minio_store(host, mi, gzip) {
    // Now is the right time to wait for the bucket to be created.
    if (mi.bucketPending) {
        await mi.bucketFuture;
        mi.bucketPending = false;
        if (mi.progress) {
            console.log(`MINI bucket '${mi.bucket}' available`);
        }
    }

    let hostelts = host.split('.');
    hostelts.reverse();
    let hrev = hostelts.join('.');
    let dir  = hostelts.join('/');
    if (dir.slice(-4) == '/www') {
        dir = dir.slice(0, -4);
    }

    // There's no way to be sure the filename doesn't already exist,
    // but embedding a timestamp is probably good enough.
    let name =
        `${dir}/${hrev}_${(new Date()).toISOString()}.har${gzip ? ".gz" : ""}`;

    // minio.Client.putObject wants to be given a readable stream, but
    // our callers want us to provide a writable stream.  If we are
    // gzipping, the gzip stream can be used as a shim; if we aren't,
    // use a passthrough transform.
    let shim = gzip
        ? zlib.createGzip({ level: 9 })
        : new stream.PassThrough();

    // Create a wrapper writable that fires 'finished' only when the
    // shim is finished _and_ putObject has completed.
    return {
        stream: new stream.Writable({
            write(d, e, c) {
                // kludge to force the outer stream to be fully "drained"
                // at all times; there's probably a better way...
                shim.write(d, e); c();
            },
            final(c)       {
                shim.end();
                if (mi.progress) {
                    console.log(`MINI uploading ${name}`);
                }
                mi.client.putObject(mi.bucket, name, shim).then(() => {
                    if (mi.progress) {
                        console.log(`MINI upload complete for ${name}`);
                    }
                    c();
                }, (e) => c(e));
            }
        }),
        oname: mi.base_url + name
    };
}

//
// helpers
//
function make_invalid_url_report(url) {
    let timestamp = (new Date()).toISOString();
    return {
        log: {
            version: "1.2",
            creator: {name: "ICLab Uncensored Page Capture",
                      version: "0.1"},
            pages: [ {
                id: "p",
                title: url,
                startedDateTime: timestamp,
                pageTimings: { onContentLoad: -1, onLoad: -1 }
            } ],
            entries: [ {
                pageref: "p",
                startedDateTime: timestamp,
                time: 0,
                request: {
                    method: "GET",
                    url: url,
                    httpVersion: "unknown",
                    cookies: [],
                    headers: [],
                    queryString: [],
                    headersSize: -1,
                    bodySize: 0
                },
                response: {
                    statusText: "net::ERR_INVALID_URL",
                    httpVersion: "unknown",
                    cookies: [],
                    headers: [],
                    redirectURL: "",
                    headersSize: -1,
                    bodySize: -1,
                    content: {
                        size: 0
                    },
                    _security: {}
                },
                cache: {},
                timings: {
                    blocked: -1,
                    dns: -1,
                    connect: -1,
                    send: -1,
                    wait: -1,
                    receive: -1,
                    ssl: -1
                }
            } ]
        }
    };
}

class Output {
    constructor(options) {
        this.dir = path.normalize(options.dir || ".");
        this.compress = options.compress;
        if (options.s3) {
            this.mc = minio_client_from_options(options, this.dir);
        } else {
            mkdirp.sync(this.dir);
            this.mc = null;
        }
    }
    prepare(hostname) {
        if (this.mc !== null) {
            return minio_store(hostname, this.mc, this.compress);
        } else {
            return disk_store(hostname, this.dir, this.compress);
        }
    }
    async invalid_url_report(url, err) {
        let href, hostname;
        if (url.href !== undefined) {
            href = url.href;
            hostname = url.hostname + ".invalid";
        } else {
            href = url;
            // best-effort extract a hostname from an ill-formed URL
            let m=/^(?:[a-z]*(?::+\/*|\/+))?(?:[^@/\n]+?@)?([^/\n]+)(?=(?:\/|$))/i.exec(url);
            if (m !== null) {
                hostname = m[1] + ".invalid";
            } else {
                hostname = "unknown.invalid";
            }
        }
        let har = make_invalid_url_report(href);
        let {stream, oname} = await this.prepare(hostname);
        await new Promise((res, rej) => {
            stream.on('finish', () => res());
            stream.on('error', (err) => {
                // not much we can realistically do here
                console.error(err);
                res();
            });
            stream.write(JSON.stringify(har, null, this.indent));
            stream.end('\n');
        });
        return oname;
    }
}

module.exports = Output;

// Local Variables:
// mode: js2
// js2-skip-preprocessor-directives: t
// js2-include-node-externs: t
// End:
