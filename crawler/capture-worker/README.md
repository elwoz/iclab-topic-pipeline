# Notes on the Capture Worker

The capture worker controls one instance of [Headless Chrome][hc]
and puppets it through the process of collecting all of the data we
are interested in saving from the uncensored version of each website.
This data is recorded in the form of a [HAR archive][har], one file
per URL.

## Command-line usage

When invoked normally from the command line, the capture worker takes
a list of URLs as arguments, and writes out one HAR file per URL to
the current working directory.  The name of the HAR file is derived
from the URL and will include at least the hostname.

## Usage with the orchestrator

TBD

## Information we record

By default, images and other media files are _not_ loaded; only HTML,
CSS, and JavaScript.

We record each HTTP request and response, whether or not it is
successful; we also record the 


[hc]: ...
[har]: 
